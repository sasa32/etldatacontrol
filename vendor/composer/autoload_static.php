<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit2fe911c8bc2cc952bc2d590b3296a3b4
{
    public static $prefixLengthsPsr4 = array (
        'O' => 
        array (
            'Office365\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Office365\\' => 
        array (
            0 => __DIR__ . '/..' . '/vgrem/php-spo/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit2fe911c8bc2cc952bc2d590b3296a3b4::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit2fe911c8bc2cc952bc2d590b3296a3b4::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit2fe911c8bc2cc952bc2d590b3296a3b4::$classMap;

        }, null, ClassLoader::class);
    }
}
