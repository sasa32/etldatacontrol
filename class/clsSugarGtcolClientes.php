<?php 

require_once 'vendor/autoload.php';

use Office365\Runtime\Auth\UserCredentials;
use Office365\SharePoint\ClientContext;
use Office365\SharePoint\FileCreationInformation;

/**
 * 
 */
class clsSugarGtcolClientes
{

	var $config;
	var $url;
	var $mysql;
	var $session_id;
	var $login_parameters;	
	var $adjuntos;	
	var $drive_parent;
	var $hora_inicio;
	var $http_code;
	var $mysql_query;
	
	function __construct()
	{
		try {
			$hora_inicio = new DateTime();

			require_once("conf/conf.php");
			$this->config = $config;

			require_once("dbMysql.php");
			$this->mysql=new dbMysql($this->config["mysql"]);
			//$this->mysql_query = $this->mysql->connect($this->config["mysql"]);
			$this->login_parameters = array(
				"grant_type"=>$this->config['sugar']['grant_type'], 
				"client_id"=>$this->config['sugar']['client_id'], 
				"client_secret"=>$this->config['sugar']['client_secret'], 
				"username"=>$this->config['sugar']['username'], 
				"password"=>$this->config['sugar']['password'], 
				"platform"=>$this->config['sugar']['platform']
			);
		} catch (Exception $e) {
			echo "\nERROR1:".$e->getMessage()."\n";
		}
	}

	function elapsed_time(){
		$first = $this->hora_inicio;
		$second = new DateTime();

		$diff = $first->diff( $second );

		echo "\nelapsed_time: ".$diff->format( '%H:%I:%S' )."\n"; // -> 00:25:25
	}

	function ObtenerData(){
		try {
			$return=true;
			echo "\nObtener archivos de Sharepoint";
			//Credenciales de Sharepoint
			$credentials = new UserCredentials($this->config['sharepoint']['UserName'], $this->config['sharepoint']['Password']);
			//Ubicando el sitio
			$ctx = (new ClientContext($this->config['sharepoint']['Url']))->withCredentials($credentials);

			$rootFolder = $ctx
			    ->getWeb()
			    ->getFolderByServerRelativeUrl($this->config['sharepoint']['eventos_directory'])
			    ->expand('Files')
			    ->get()
			    ->executeQuery();


			/** @var File $file */
			foreach ($rootFolder->getFiles() as $file) {
			    try {
			        //echo "\nFIlename {$file->getName()}";
			        $localPath = join(DIRECTORY_SEPARATOR, [$this->config['file']['files_path_local'], $file->getName()]);
			        $fh = fopen($localPath, $this->config['file']['files_path_local_permiso']);
			        $file->download($fh)->executeQuery();
			        fclose($fh);
			        echo "\nFile: {$file->getServerRedirectedUrl()} has been downloaded into {$localPath}\r\n";
			        //Instanciar CSV_Class, clase para todo el manejo del csv, creación de tabla temporal e inserción de data a la tabla
			        require_once("CSV_Class.php");
					$csv_class = new CSV_Class($this->config,$file->getName());
					//Si se pudo procesar el archivo, crear tabla e insertar datos en la misma
					if ($csv_class->status) {
						// code...
						echo "\nTERMINOOOO";
						$this->process_send_data($csv_class->table);

						$this->delete_table_file($csv_class->table,$localPath);
					}else{
						echo "\n ERROR al instanciar la clase CSV_Class";
					}
			    } catch (Exception $ex) {
			        echo "\nError {$ex->getCode()} - File download failed: {$ex->getMessage()}";
			    }
			}

			return $return;
		} catch (Exception $e) {
			echo "\nERROR2:".$e->getMessage()."\n";
		}
	}

	private function delete_table_file($table,$path_file){
		$bd = new dbMysql($this->config["mysql"]);
		echo "Borrando Tabla temp_{$table}";
		//Eliminar contenido de la tabla que se está procesando
		$query_drop_table = "DROP TABLE IF EXISTS temp_{$table}";
		$result = $bd->query($query_drop_table);

		//Eliminar archivo del server
		unlink($path_file);
	}

	function process_send_data($table){
		$table_name = "temp_".$table;
		if($table_name !== "temp_"){
			echo "\n[".date('Y-m-d G:i:s')."][SUCCESS] -> Generando JSON desde la tabla: {$table_name} \n"; 
			$sql_table_limit = "SELECT count(*) FROM {$table_name}";
			$bd = new dbMysql($this->config["mysql"]);
			$limit = $bd->select($sql_table_limit);
			#Se trae el array con el nombre de las columnas, que deben ser exactas a las que se va a enviar a SugarCRM
			$sql_columns_name = "SHOW COLUMNS FROM {$table_name}";
			$columns_name = $bd->select($sql_columns_name);
			
			if($limit[0][0]){
				#Existe un número total de registros en la tabla seleccionada, se procede a páginar la tabla para generar los json necesarios
				$total_registers_send = 0;
				for ($i=0; $i < $limit[0][0]; $i+=$this->config['csv']['paging']){
					$count_sql_select_pag ="SELECT count(*) FROM {$table_name} LIMIT {$i},".$this->config['csv']['paging'];
					$sql_select_pag = "SELECT * FROM {$table_name} LIMIT ". $i.",".$this->config['csv']['paging'];
					echo "\n QUERY 2".$sql_select_pag;
					$limit_sql_select_pag = $bd->select($count_sql_select_pag);
					$data = $bd->select($sql_select_pag);
					//echo "\nDATASQL".print_r($data,true);
					$json_request = array();
					$count = 0;
					for ($k=0; $k <$limit[0][0] ; $k++) {
					//for ($k=0; $k <$this->config['csv']['paging'] ; $k++) { 
						for ($j=0; $j <count($columns_name) ; $j++) {
							if($j != 0){
								$json_request[$count+$k][$columns_name[$j][0]] = $data[$k][$j];
							}
						}
					}
					$total_registers_send += count($json_request); #Sumatoria de registros enviados al WS
					$json_request = array_map(function($json_request){
						return array(
							'Nombre_cliente' => utf8_encode($json_request['Nombre_cliente']),
							'NIT' => utf8_encode($json_request['NIT']),
							'Nombre_Contacto' => utf8_encode($json_request['Nombre_Contacto']),
							'Apellido_Contacto' => utf8_encode($json_request['Apellido_Contacto']),
							'NumeroID_Contacto' => utf8_encode($json_request['NumeroID_Contacto']),
							'email_contacto' => utf8_encode($json_request['email_contacto']),
							'email_cuenta' => utf8_encode($json_request['email_cuenta']),
							'Medio_Evento' => utf8_encode($json_request['Medio_Evento']),
							'Ciudad' => utf8_encode($json_request['Ciudad']),
							'Departamento' => utf8_encode($json_request['Departamento']),
							'Pais' => utf8_encode($json_request['Pais']),
							'Cargo' => utf8_encode($json_request['Cargo']),
							'telefono' => utf8_encode($json_request['telefono']),
							'evento' => utf8_encode($json_request['evento']),
							'estado' => utf8_encode(preg_replace("/[\r\n|\n|\r|\s]+/", "", $json_request['estado']))
						);
					},$json_request);

					$json_body = array(
						"data" => $json_request
					);

					//echo "\nJSONBODY ".print_r($json_body,true);

					$respond_api = json_decode($this->sent_your_json($json_body));
					//echo "\nJSON ".json_decode($json_body,true);//print_r($json_body,true);
					echo "\n RESPONSE ".print_r($respond_api,true);
				}
			}
		}
	}

	/*
	* Función utilizada para consumir el Web Service de autenticación de SugarCRM
	*/
	private function get_access_token(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "{$this->config['sugar']['site_url']}{$this->config['endpoint']['autenticar_ws']}",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 90,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode(
				array(
					"grant_type" => "password",
					"client_id" => "{$this->config['sugar']['client_id']}",
					"client_secret" => "{$this->config['sugar']['client_secret']}",
					"username" => "{$this->config['sugar']['username']}",
					"password" => "{$this->config['sugar']['password']}",
					"platform" => "{$this->config['sugar']['platform']}"
				)
			),
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "Error Token: ".json_decode($err);
			return array('error' => json_decode($err));
		}else{
			echo "Token: ".print_r($response,true);
			return json_decode($response);
		}
	}

	/*
	* Función encargada de enviar el body a un Web Service POST de SugarCRM
	* Argumento solicitado es un array, con la estructura json que se va a enviar, en esta función se transforma con un 
	* JSON encode para convertir el array que llega a un JSON
	*/
	private function sent_your_json($my_json_in_array){
		$access_token = $this->get_access_token();
		if(!empty($access_token->access_token)){

			$curl = curl_init();
			echo "JSONSEND ".json_encode($my_json_in_array,JSON_FORCE_OBJECT);
			curl_setopt_array($curl, array(
				CURLOPT_URL => "{$this->config['sugar']['site_url']}{$this->config['endpoint']['ws_datacontrol']}",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 90,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => json_encode($my_json_in_array),
				CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"oauth-token: {$access_token->access_token}" 
				),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);
			echo "\n{$this->config['SugarCRM']['intance_url']}";
			if ($err) {
				echo "[".date('Y-m-d G:i:s')."][ERROR] -> Problemas con la ejecución REST: {$err}\n";
				return array('Error' => $err);
			} else {
				return $response;
			}
		}else{
			echo "[".date('Y-m-d G:i:s')."][ERROR] -> Access Token no valido\n";
			return array('Error' => "No hay access-token");
		}
	}

}

 ?>
