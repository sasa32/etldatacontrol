<?php 

class dbMysql {
	protected static $connection;
	
    function __construct($c)
    {
        /*if(!isset(self::$connection)) {
            self::$connection = mysqli_connect($c['host'],$c['user'],$c['password'],$c['new_link'],$c['client_flags']);
        }

        if(self::$connection === false) {
            return false;
        }
        mysqli_select_db(self::$connection,$c['database']);
        return self::$connection;*/
        try {
            echo "[".date('Y-m-d G:i:s')."][SUCCESS] -> Iniciando Conexión BD \n";
            self::$connection = new mysqli($c['host'],$c['user'],$c['password'], $c['database']);
            
            if(self::$connection->connect_error){
                echo "[".date('Y-m-d G:i:s')."][ERROR] -> No se pudo conectar con la BD ".self::$connection->connect_error."\n";
                return false;
            }else{
                echo "[".date('Y-m-d G:i:s')."][SUCCESS] -> Conexión a la BD->{$this->name_bd} establecida\n";
                return true;
            }
        } catch (Exception $e) {
            echo "[".date('Y-m-d G:i:s')."][ERROR] -> Se tiene problemas con la conexión a la BD: ".$e->getMessage()."\n";
        }
    }

    public function query($query) {
        $result =  self::$connection->query($query);
        if($result) {
            return $result;
        }else{
            echo "[".date('Y-m-d G:i:s')."][ERROR] -> Hay un error en el query enviado: ".$this->error()."\n";
            return array('error' => $this->error());
            
        }
    }

    public function select($query) {
        if(self::$connection){
            $result = self::$connection->query($query);
            if($result){
                $return = array();
                while($row = $result->fetch_row()){
                    $return[] = $row;
                }
                return $return;
            }else{
                return array('error' => $this->error());
            }  
        }else{
            echo "[".date('Y-m-d G:i:s')."][ERROR] -> No se establecio conexión a la BD"."\n";
            return array('error' => "[".date('Y-m-d G:i:s')."][ERROR] -> No se establecio conexión a la BD"); 
        }
    }

    public function error() {
        return self::$connection->error;
    }

    public function close_BD(){
        try {
            if(self::$connection === null){
                echo "[".date('Y-m-d G:i:s')."][ERROR] -> No hay conexión a la BD";
            }else{
                mysqli_close(self::$connection);  
            }
        } catch (Exception $e) {
            echo "[".date('Y-m-d G:i:s')."][ERROR] -> Hay un problema cerrando la BD: ".$e->getMessage()."\n";
        }
    }

	/*public function connect($c) {    
        
        if(!isset(self::$connection)) {
            
             
            self::$connection = mysqli_connect($c['host'],$c['user'],$c['password'],$c['new_link'],$c['client_flags']);
			
        }


        if(self::$connection === false) {

            return false;
        }
	mysqli_select_db(self::$connection,$c['database']);
        return self::$connection;
    }*/


    /*public function query($query) {
        // Connect to the database
        //$connection = $this -> connect();

        // Query the database
        $result =  mysqli_query(self::$connection,$query);

	if($result === false) {
            return $this->error();
        }	
		
        return $result;
    }*/

    /*public function select($query) {
        $rows = array();
        $result = $this -> query($query);
        if($result === false) {
            return $this->error();
        }
        while ($row =mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
		
        return $rows;
    }*/

    
    /*public function error() {
        //$connection = $this -> connect();
        return mysqli_errno(self::$connection);
    }*/

    /*public function quote($value) {
        $connection = $this -> connect();
        return "'" . self::$connection->real_escape_string($value). "'";
    }*/

	
	
	
	
}

?>