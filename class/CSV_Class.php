<?php 

//Clase encargada para procesar los csv, leer e insertar la data de los csv
class CSV_Class
{
	var $conf;
	var $table;
	var $file_name;
	var $status;

	function __construct($conf,$file_name)
	{
		$this->conf = $conf;
		$this->file_name = $file_name;
		$this->insert_data();
	}

	function insert_data(){
		try {
			if ($this->file_name) {
				$file_dir_and_file_name = $this->conf['csv']['dir'].$this->file_name;

				echo "PATH FILE ".$file_dir_and_file_name;

				$file  = fopen($file_dir_and_file_name, "r");
				if ($file) {
					//Traer la data
					$data = fgetcsv($file, $this->conf['csv']['char_separate']);
					$columns = explode($this->conf['csv']['char_separate'], $data[0]);

					//Obtener nombre de la tabla basandose en el nombre del archivo a procesar
					$table_name = str_replace(['.csv','.txt', '.', '/', "\n"], '', $this->file_name);
					if ($table_name != "") {
						//Borrar la tabla en caso de existir
						$drop_table_sql = "DROP TABLE IF EXISTS temp_{$table_name};";
						//Crear la tabla con el nombre del archivo identificado
						$create_table_sql = "CREATE TABLE IF NOT EXISTS temp_{$table_name} (id_register INT NOT NULL AUTO_INCREMENT PRIMARY KEY, ";
						//Crear columnas basadas en el csv procesado
						foreach ($columns as $key => $value) {
							$colunm_name = str_replace($this->conf['csv']['char_separate'], '', $value);
							if($key !== (count($columns) - 1) AND !empty($colunm_name)){
								$create_table_sql .= "{$colunm_name} VARCHAR({$this->conf["csv"]["size_data"]}),";
							}else if(!empty($colunm_name)){
								$create_table_sql .= "{$colunm_name} VARCHAR({$this->conf["csv"]["size_data"]}))";
							}
						}

						require_once("dbMysql.php");
						$db = new dbMysql($this->conf["mysql"]);
						//Borrar la tabla en caso de existir
						$execute_drop_table = $db->query($drop_table_sql);
						$execute_create_table = $db->query($create_table_sql);
						if ($execute_create_table) {
							$load_data_on_temp_table_sql = "LOAD DATA LOCAL INFILE '{$file_dir_and_file_name}' INTO TABLE temp_{$table_name} FIELDS TERMINATED BY '{$this->conf['csv']['char_separate']}' LINES TERMINATED BY '\\n' STARTING BY '' IGNORE {$this->conf['csv']['ignore']} LINES (" ;
							#Columnas a insertar
							foreach ($columns as $key => $value) {
								$colunm_name = str_replace($this->conf["CSV"]["char_separate"], '', $value);
								if($key !== (count($columns) - 1)){
									$load_data_on_temp_table_sql .= "@{$colunm_name}, ";
								}else{
									$load_data_on_temp_table_sql .= "@{$colunm_name}) SET ";
								}
							}
							#Para insertar todo el CSV en la tabla temporal que se creo
							foreach ($columns as $key => $value) {
								$colunm_name = str_replace(';', '', $value);
								if($key !== (count($columns) - 1)){
										$load_data_on_temp_table_sql .= "{$colunm_name} = @{$colunm_name}, ";
								}else{
									$load_data_on_temp_table_sql .= "{$colunm_name} = @{$colunm_name}";
								}
							}
							$result_load_data = $db->query($load_data_on_temp_table_sql);
							if($result_load_data == false){
								echo "\n[".date('Y-m-d G:i:s')."][ERROR] -> No se realiza carga de los registros";
							}else{
								$this->status = true;
								$this->table = $table_name;
								echo "\n[".date('Y-m-d G:i:s')."][SUCCESS] -> Carga realizada";
							}

						}else{
							echo "\nERROR No se pudo crear la tabla";
						}
					}else{
						echo "\nERROR No se encontró nombre para la tabla";
					}
					fclose($file);
				}else{
					echo "[".date('Y-m-d G:i:s')."][ERROR] -> No se encuentra el archivo CSV\n";
				}
			}else{
				echo "[".date('Y-m-d G:i:s')."][ERROR] -> No se ha logrado encontrar el archivo solicitado\n";
			}
		} catch (Exception $e) {
			echo "[".date('Y-m-d G:i:s')."][ERROR] -> Hubo un problema en la carga del CSV:".$e->getMessage()."\n";
		}
	}
}

 ?>