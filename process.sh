#!/bin/bash
source /etc/environment 
DAY=$(date +"%d-%m-%Y_%H:%M:%S");
PROCESO="process";
BASE="/home/sugargtco/SugarGtcolClientes";
URL=$(date +"$BASE/logs/$DAY.log");
#URL=$(date +"$BASE/logs/$PROCESO/SQA.log");printf "\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n\\n" > "$URL";
mkdir -p "$BASE/logs";
ORIGEN="sharepoint";
{
	echo "*************************************************************************" 	&>> "$URL";
	echo "<<<<<<<<<<<<-- 	INICIO $PROCESO $ORIGEN to sugar 	-->>>>>>>>>>>>>"	&>> "$URL";
	echo "*******************************`date`************************************" 	&>> "$URL";
	cd $BASE										&>> "$URL";
	php -f "$BASE/$PROCESO.php" "$PROCESO $ORIGEN $DAY"				       		&>> "$URL";
	echo "*******************************`date`************************************" 	&>> "$URL";
	echo "<<<<<<<<<<<<-- 	FIN $PROCESO $ORIGEN to sugar 		-->>>>>>>>>>>>>"	&>> "$URL";
	php -f "$BASE/mail.php" "$URL" "$PROCESO $ORIGEN $DAY"					&>> "$URL";
	echo "*************************************************************************" 	&>> "$URL";
} || { echo "fallo proceso batch" 	&>> "$URL"; }